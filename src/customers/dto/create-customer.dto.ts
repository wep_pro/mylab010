import { IsNotEmpty, IsPositive, Length } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(2, 50)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  age: number;

  @IsNotEmpty()
  @Length(10, 10)
  tel: string;

  @IsNotEmpty()
  @Length(1, 7)
  gender: string;
}
